package top.dbwxd.f;

import java.io.Serializable;

/**
 * @author xiaodongw Date: 2019-01-15 Time: 20:28
 * @version $Id$
 * 多使用静态工厂方法
 */
public class StaticFactoryVo implements Serializable {


    private StaticFactoryVo() {
    }

    private StaticFactoryVo(String name) {
        this.name = name;
    }

    private String name;

    private Long id;

    private String desc;

    private Long uuid;

    public static StaticFactoryVo newInstance() {
        return new StaticFactoryVo();
    }

    public static StaticFactoryVo newInstance(String name) {
        return new StaticFactoryVo(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "StaticFactoryVo{" +
            "name='" + name + '\'' +
            ", id=" + id +
            ", desc='" + desc + '\'' +
            ", uuid=" + uuid +
            '}';
    }
}
