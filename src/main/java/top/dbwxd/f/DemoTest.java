package top.dbwxd.f;

import com.alibaba.fastjson.JSON;

/**
 * @author xiaodongw Date: 2019-01-15 Time: 20:37
 * @version $Id$
 */
public class DemoTest {

    public static void main(String[] args) {
        StaticFactoryVo vo = StaticFactoryVo.newInstance();
        vo.setName("abc");
        System.out.println(vo);

        StaticFactoryVoNo voNo = StaticFactoryVoNo.newInstance();
        voNo.setName("fwf");
        System.out.println(JSON.toJSONString(voNo));
    }
}
