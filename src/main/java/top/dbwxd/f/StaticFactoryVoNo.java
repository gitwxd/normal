package top.dbwxd.f;

import java.io.Serializable;

/**
 * @author xiaodongw Date: 2019-01-15 Time: 20:28
 * @version $Id$
 */
public class StaticFactoryVoNo {

    private StaticFactoryVoNo() {
    }

    private StaticFactoryVoNo(String name) {
        this.name = name;
    }

    private String name;

    private Long id;

    private String desc;

    private Long uuid;

    public static StaticFactoryVoNo newInstance() {
        return new StaticFactoryVoNo();
    }

    public static StaticFactoryVoNo newInstance(String name) {
        return new StaticFactoryVoNo(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }


}
