package top.dbwxd.g.consumer;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import top.dbwxd.g.GreetingService;

/**
 * @author xiaodongw Date: 2019-01-25 Time: 10:34
 * @version $Id$
 */
public class Application {

    public static void main(String[] args) {
        ReferenceConfig<GreetingService> referenceConfig = new ReferenceConfig<GreetingService>();
        referenceConfig.setApplication(new ApplicationConfig("first-dubbo-consumer"));
        referenceConfig.setRegistry(new RegistryConfig("zookeeper://192.168.174.128:2181"));
        System.out.println("=====1");
        referenceConfig.setInterface(GreetingService.class);
        System.out.println("=====2");
        GreetingService greetingService = referenceConfig.get();
        System.out.println("=====3");
        System.out.println(greetingService.sayHello("world"));
        System.out.println("end");
    }
}
