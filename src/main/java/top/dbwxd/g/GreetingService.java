package top.dbwxd.g;

/**
 * @author xiaodongw Date: 2019-01-25 Time: 10:06
 * @version $Id$
 */
public interface GreetingService {

    String sayHello(String name);
}
