package top.dbwxd.g.provider;

import top.dbwxd.g.GreetingService;

/**
 * @author xiaodongw Date: 2019-01-25 Time: 10:07
 * @version $Id$
 */
public class GreetingServiceImpl implements GreetingService {

    @Override
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
