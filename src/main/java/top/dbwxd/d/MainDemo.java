package top.dbwxd.d;

import org.springframework.aop.framework.ProxyFactory;

/**
 * @author xiaodongw Date: 2019-01-10 Time: 10:56
 * @version $Id$
 */
public class MainDemo {

    public static void main(String[] args) {
        //静态代理
        GreetProxy proxy = new GreetProxy(new GreetImpl());
        proxy.sayHello("wxd");

        //jdk动态代理
        Greet greet = new JDKDynamicProxy(new GreetImpl()).getProxy();
        greet.sayHello("jackson");

        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTarget(new GreetImpl());
        proxyFactory.addAdvice(new GreetingBeforeAdvice());
        proxyFactory.addAdvice(new GreetingAfterAdvice());
        Greet greet1 = (Greet) proxyFactory.getProxy();
        greet1.sayHello("jackson w ");
    }
}
