package top.dbwxd.d;

/**
 * @author xiaodongw Date: 2019-01-10 Time: 10:53
 * @version $Id$
 * 使用静态代理
 * 存在的问题 类似这样的代理类会越来越多
 */
public class GreetProxy implements Greet {

    private GreetImpl greet;

    public GreetProxy(GreetImpl greet) {
        this.greet = greet;
    }

    @Override
    public void sayHello(String name) {
        before();
        greet.sayHello(name);
        after();
    }

    private void before() {
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }
}
