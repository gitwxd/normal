package top.dbwxd.d;

import org.springframework.stereotype.Component;

/**
 * @author xiaodongw Date: 2019-01-10 Time: 10:50
 * @version $Id$
 * 不理想的写法case
 */
@Component
public class GreetImpl implements Greet {

    @Override
    public void sayHello(String name) {
        System.out.println("hello:" + name);
    }

    private void before() {
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }

    //测试切面
    public void goodMorning(String name) {
        System.out.println("Good Morning " + name);
    }

    public void goodNight(String name) {
        System.out.println("Good Night " + name);
    }
}
