package top.dbwxd.d;

/**
 * @author xiaodongw Date: 2019-01-10 Time: 10:50
 * @version $Id$
 */
public interface Greet {
    void sayHello(String name);
}
