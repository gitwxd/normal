package top.dbwxd.j;

import com.alibaba.fastjson.JSON;
import java.util.HashMap;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 14:27
 * @version $Id$
 */
public class HashConflict {

    public static void main(String[] args) {
        HashMap<String, Object> m = new HashMap<>();
        m.put("a", "rrr1");
        m.put("b", "tt9");
        m.put("c", "tt8");
        m.put("d", "g7");
        m.put("e", "d6");
        m.put("f", "d4");
        m.put("g", "d4");
        m.put("h", "d3");
        m.put("i", "d2");
        m.put("j", "d1");
        m.put("k", "1");
        m.put("o", "2");
        m.put("p", "3");
        m.put("q", "4");
        m.put("r", "5");
        m.put("s", "6");
        m.put("t", "7");
        m.put("u", "8");
        m.put("v", "9");

        System.out.println(m);
        System.out.println(JSON.toJSON(m));
    }
}
