package top.dbwxd.j;

/**
 * @author xiaodongw Date: 2019-02-12 Time: 21:30
 * @version $Id$
 */
public class HashCodeDemo {

    /**测试hashcode 字符串类型是与内容相关的 内容相同hashcode相同*/
    public static void main(String[] args) {
        String s = new String("hello");
        StringBuilder sb = new StringBuilder(s);
        System.out.println("s hashcode=" + s.hashCode());
        System.out.println("sb hashcode=" + sb.hashCode());
        String t = new String("hello");
        StringBuilder tb = new StringBuilder(t);
        System.out.println("t hashcode=" + t.hashCode());
        tb.hashCode();
        System.out.println("tb hashcode=" + tb.hashCode());
    }

    public static void run(){
        main(null);
    }
}
