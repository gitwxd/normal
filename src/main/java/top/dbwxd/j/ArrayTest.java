package top.dbwxd.j;

import com.alibaba.fastjson.JSON;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Name;

/**
 * @author xiaodongw Date: 2019-02-13 Time: 10:53
 * @version $Id$
 */
public class ArrayTest {

    private static final String[] name={};

    private static final int[] age={1,2,3,4,5,10};

    public static void main(String[] args) throws Exception{
        //age[age.length+1]=12;
        System.out.println(JSON.toJSON(age));
        System.out.println(JSON.toJSON(name));
        List<Integer> test=new ArrayList<>();
        test.add(1);
//        test.set(3,8);
//        test.get(2);
        System.out.println(test);

        System.out.println("-------------");
        Class<? extends List> a = test.getClass();
        System.out.println(a);
        ClassLoader classLoader = a.getClassLoader();
        System.out.println(classLoader);
        Constructor<?>[] constructors = a.getConstructors();
        System.out.println(constructors);
        Class<List> listClass = List.class;
        HashCodeDemo d=new HashCodeDemo();
        HashCodeDemo demo = d.getClass().newInstance();
        demo.run();
    }
}
