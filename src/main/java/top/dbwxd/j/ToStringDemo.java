package top.dbwxd.j;

/**
 * @author xiaodongw Date: 2019-02-12 Time: 21:54
 * @version $Id$
 */
public class ToStringDemo {

    public static class Student {

        private int age;
        private String name;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

//        @Override
//        public String toString() {
//            return "Student{" +
//                "age=" + age +
//                ", name='" + name + '\'' +
//                '}';
//        }

                @Override
        public String toString() {
            return getClass().getName()+"{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
        }
    }

    public static void main(String[] args) {
        Student s=new Student();
        s.setAge(22);
        s.setName("wangixaod");
        System.out.println(s);
    }
}
