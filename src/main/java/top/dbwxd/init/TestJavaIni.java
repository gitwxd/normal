package top.dbwxd.init;

/**
 * @author xiaodongw Date: 2019-05-10 Time: 6:27
 * @version $Id$
 */
public class TestJavaIni {

    private static User user = new User();

    static {
        System.err.println("static code block");
    }


    {
        System.err.println("code block");
    }

    private Student student = new Student();

    public TestJavaIni() {
        System.err.println("Constructor");
    }

    public static void main(String[] args) {
        System.err.println("mian ==>");
        new TestJavaIni();
    }
}

class Student {

    public Student() {
        System.err.println("student initint===>");
    }
}

class User {

    public User() {
        System.err.println("user initing===>");
    }

}
