package top.dbwxd;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @author xiaodongw Date: 2019-05-05 Time: 18:09
 * @version $Id$
 */
public class BSearch {

    public static void main(String[] args) {
        Integer[] wit = {0, 0, 3, 4, 5, 9, 12};
        Integer search = 4;
        Integer index = search(wit, search);
        System.out.println("end:");
        System.out.println(index);
    }

    private static Integer search(Integer[] arr, Integer a) {
        int lowIndex = 0;
        int highIndex = arr.length - 1;
        int mid;
        while (lowIndex <= highIndex) {
            mid = (lowIndex + highIndex) / 2;
            if (arr[mid] == a) {
                return mid + 1;
            } else if (arr[mid] < a) {
                lowIndex = mid + 1;
            } else {
                highIndex = mid + 1;
            }
        }
        return -1;
    }

}
