package top.dbwxd.i;

/**
 * @author xiaodongw Date: 2019-02-12 Time: 15:52
 * @version $Id$
 */
public class JavaVMStackSOF {


    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        System.out.println("aaa");
        stackLeak();
    }


    public static void main(String[] args) throws Throwable {
        JavaVMStackSOF oom = new JavaVMStackSOF();
        try {
            oom.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length=" + oom.stackLength);
            throw e;
        }
    }
}

