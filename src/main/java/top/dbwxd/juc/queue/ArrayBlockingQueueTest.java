package top.dbwxd.juc.queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 17:09
 * @version $Id$
 */
public class ArrayBlockingQueueTest {

    public static void main(String[] args) throws Exception {
        System.out.println("begin");
        ArrayBlockingQueue<String> abq = new ArrayBlockingQueue<>(10);
        System.out.println("begin insert");
        // insertBlocking();
        // fetchBlocking();
        testProducerConsumer(abq);
    }

    public static void insertBlocking() throws Exception {
        ArrayBlockingQueue<String> names = new ArrayBlockingQueue<>(2);
        names.put("a");
        System.out.println("put a 执行完毕");
        names.put("b");
        System.out.println("put b 执行完毕");//执行不到 阻塞
        System.out.println("insertBlocking ended====");
    }

    public static void fetchBlocking() throws Exception {
        ArrayBlockingQueue<String> names = new ArrayBlockingQueue<>(2);
        names.put("a");
        System.out.println("put a end");
        names.remove();
        System.out.println("first time remove over");
        names.remove(); //报错  java.util.NoSuchElementException
        System.out.println("second time remove over");
        names.put("b");
        System.out.println("fetchBlocking end ===");

    }

    public static void testProducerConsumer(ArrayBlockingQueue<String> abq) {
        Thread tConsumer = new Consumer(abq);
        Thread tProducer = new Producer(abq);
        tConsumer.start();
        tProducer.start();
    }

}
