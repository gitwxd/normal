package top.dbwxd.juc.queue.CopyOnWriteArrayList;

import java.util.List;

/**
 * @author xiaodongw Date: 2019-02-15 Time: 18:11
 * @version $Id$
 */
public class ReadThread implements Runnable {

    private List<Integer> list;

    public ReadThread(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {

        for (Integer i : list) {
            System.out.println("ReadThread:" + i);
        }
    }
}
