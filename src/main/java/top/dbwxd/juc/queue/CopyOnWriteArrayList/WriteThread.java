package top.dbwxd.juc.queue.CopyOnWriteArrayList;

import java.util.List;

/**
 * @author xiaodongw Date: 2019-02-15 Time: 18:13
 * @version $Id$
 */
public class WriteThread implements Runnable{

    private List<Integer>list;

    public WriteThread(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        list.add(999);
    }
}
