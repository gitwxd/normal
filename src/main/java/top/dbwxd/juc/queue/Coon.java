package top.dbwxd.juc.queue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 15:13
 * @version $Id$
 */
public class Coon {

    public static void main(String[] args) throws Exception {
        Queue<String> queue = new ConcurrentLinkedQueue<>();
        for (int i = 0; i < 1000000; i++) {
            queue.add(String.valueOf(i));
        }

       // queue.offer("abc");
        int num = 10;//线程个数
        for (int i = 0; i < num; i++) {
            new ThreadConn(queue);
        }

    }
}
