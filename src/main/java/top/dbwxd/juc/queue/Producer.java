package top.dbwxd.juc.queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 17:20
 * @version $Id$
 */
public class Producer extends Thread {

    ArrayBlockingQueue<String> abq = null;

    public Producer(ArrayBlockingQueue<String> abq) {
        this.abq = abq;
    }

    @Override
    public void run() {
        int i = 1;
        while (true) {
            try {
                Thread.sleep(500);
                abq.put("" + i);
                System.out.println("存放数据：====" + i + "\t当前数据量：" + abq.size() + "\t空闲量：" + (10 - abq.size()));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
