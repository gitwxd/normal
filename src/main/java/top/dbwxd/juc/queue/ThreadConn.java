package top.dbwxd.juc.queue;

import java.util.Queue;
import org.joda.time.DateTime;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 15:13
 * @version $Id$
 * 启动10个线程的效率不如1个线程
 * 如果业务逻辑的耗时能达到1毫秒以上，多线程是有意义的
 * */
public class ThreadConn implements Runnable {

    Queue<String> queue;

    public ThreadConn(Queue<String> queue) {
        this.queue = queue;
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            long sd = DateTime.now().getMillis();
            while (queue.poll() != null) {
                //业务逻辑
                Thread.sleep(1);
            }
            long sn = DateTime.now().getMillis();
            System.out.println(sn - sd);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
