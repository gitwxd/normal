package top.dbwxd.juc.queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author xiaodongw Date: 2019-02-14 Time: 17:18
 * @version $Id$
 */
public class Consumer extends Thread {

    ArrayBlockingQueue<String> abq = null;

    public Consumer(ArrayBlockingQueue<String> abq) {
        super();
        this.abq = abq;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
                String msg = abq.remove();
                System.out.println("取数据：====" + msg + "\t剩余数据量：" + abq.size());
            } catch (Exception e) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
