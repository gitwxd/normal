package top.dbwxd.juc.queue.ArrayDeque;

/**
 * @author xiaodongw Date: 2019-02-15 Time: 13:22
 * @version $Id$
 * 计算比numElements大的最小2的幂次方
 */
public class TwoMiTest {

    private static final int MIN_INITIAL_CAPACITY = 8;

    public static void main(String[] args) {
        calculateSize(12);
    }

    private static int calculateSize(int numElements) {
        int initialCapacity = MIN_INITIAL_CAPACITY;
        // Find the best power of two to hold elements.
        // Tests "<=" because arrays aren't kept full.
        if (numElements >= initialCapacity) {
            initialCapacity = numElements;
            initialCapacity |= (initialCapacity >>> 1);
            initialCapacity |= (initialCapacity >>> 2);
            initialCapacity |= (initialCapacity >>> 4);
            initialCapacity |= (initialCapacity >>> 8);
            initialCapacity |= (initialCapacity >>> 16);
            initialCapacity++;

            if (initialCapacity < 0)   // Too many elements, must back off
            {
                initialCapacity >>>= 1;// Good luck allocating 2 ^ 30 elements
            }
        }
        return initialCapacity;
    }

}
