package top.dbwxd.juc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaodongw Date: 2019-03-06 Time: 16:33
 * @version $Id$
 */
public class ArrayListTest {

    static List<Integer> a = new ArrayList<>();

    public static class AddList implements Runnable {


        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                a.add(i);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new AddList());
        Thread t2 = new Thread(new AddList());

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println(a);
    }


}
