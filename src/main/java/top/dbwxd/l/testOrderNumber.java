package top.dbwxd.l;

/**
 * @author xiaodongw Date: 2019-04-23 Time: 15:22
 * @version $Id$
 */
public class testOrderNumber {

    public static void main(String[] args) {
        boolean a = checkRBAOrderNumber("//ff.com");
        boolean b = checkRBAOrderNumber("12200/");
        System.out.println(a);
        System.out.println(b);
    }

    private static boolean checkRBAOrderNumber(String rbaOrderNumber) {
        if (rbaOrderNumber == null) {
            return false;
        }
        return rbaOrderNumber.matches("[+-]?[1-9]+[0-9]*(\\.[0-9]+)?");
    }
}
