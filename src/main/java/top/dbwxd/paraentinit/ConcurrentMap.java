package top.dbwxd.paraentinit;

import com.sun.prism.paint.Stop;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * @author xiaodongw Date: 2019-05-28 Time: 20:36
 * @version $Id$
 */
public class ConcurrentMap {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        ExecutorService executors = Executors.newFixedThreadPool(4);
        IntStream.range(0,1000000).forEach(i->executors.submit(atomicInteger::incrementAndGet));
        //stop(executors);
        System.out.println(atomicInteger.get());
    }
}
