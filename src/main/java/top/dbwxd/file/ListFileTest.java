package top.dbwxd.file;

import java.io.File;

/**
 * @author xiaodongw Date: 2019-03-08 Time: 18:28
 * @version $Id$
 */
public class ListFileTest {

    public static void main(String[] args) {
        System.out.println("begin");
        listAll(new File("D:\\PMO"));
    }

    public static void listAll(File dir) {
        if (dir == null || !dir.exists()) {
            System.out.println("不存在");
            return;
        }

        if (dir.isFile()) {
            System.out.println(dir.getName());
            return;
        }

        for (File file : dir.listFiles()) {
            listAll(file);
        }
    }
}
