package top.dbwxd.h;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import javafx.print.Printer;

/**
 * @author xiaodongw Date: 2019-01-31 Time: 6:12
 * @version $Id$
 */
public class InOutDemo {

    public static void main(String[] args) throws IOException {
        String abc = System.getProperty("normal/first.xml");
        System.out.println(abc);

        Scanner in = new Scanner(Paths.get("D:\\tempfile\\RBA-1517.txt"),"UTF-8");
        System.out.println(in.toString());
        while (in.hasNext()){
            String next = in.next();
            System.out.println(next);
        }

        PrintWriter printWriter = new PrintWriter("");

    }

}
