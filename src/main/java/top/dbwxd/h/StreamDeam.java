package top.dbwxd.h;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author xiaodongw Date: 2019-01-31 Time: 6:30
 * @version $Id$
 */
public class StreamDeam {

    public static void main(String[] args) throws IOException {
        String s = new String(Files.readAllBytes(Paths.get("D:\\tempfile\\RBA-1517.txt")),StandardCharsets.UTF_8);
        List<String>words= Arrays.asList(s.split("\\PL+"));
        System.out.println(JSON.toJSON(words));
        long conut=0;
        for (String w :words){
                if (w.length()>2){
                    conut++;
                }
        }
        System.out.println(conut);

        System.out.println("========流式操作");

        long l = words.parallelStream().filter(w -> w.length() > 1).count();
        System.out.println(l);

        Stream<BigInteger> iterate = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE));
        System.out.println(JSON.toJSON(iterate));
    }
}
