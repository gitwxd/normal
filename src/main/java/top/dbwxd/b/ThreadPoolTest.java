package top.dbwxd.b;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author xiaodongw Date: 2018-12-26 Time: 14:09
 * @version $Id$
 */
public class ThreadPoolTest {

    int i = 0;
    ExecutorService pushExecutor = new ThreadPoolExecutor(8, 10, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(100),
            (new ThreadFactoryBuilder())
                    .setNameFormat("PUSH_TEST_POOL-%d")
                    .build());

    ExecutorService task = new ThreadPoolExecutor(8, 10, 0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(100),
            (new ThreadFactoryBuilder())
                    .setNameFormat("PUSH_TEST_POOL-%d")
                    .build());


    public void start() {
        pushExecutor.execute(() -> {
            while (true) {
                i++;
                System.out.println(i + "====" + System.currentTimeMillis() + "=="
                        + Thread.currentThread().getName() + "==id" + Thread.currentThread().getId());
                try {

                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        task.execute(() -> {
            System.out.println("this is task" + Thread.currentThread().getId() + Thread.currentThread().getName());
        });
    }


}
