package top.dbwxd.b;

/**
 * @author xiaodongw Date: 2018-12-27 Time: 15:37
 * @version $Id$
 */
public class MainTest {

    public static void main(String[] args) {
        ThreadPoolTest poolTest = new ThreadPoolTest();
        System.out.println("begin" + Thread.currentThread().getName() + "id:" + Thread.currentThread().getId());
        poolTest.start();
    }
}
