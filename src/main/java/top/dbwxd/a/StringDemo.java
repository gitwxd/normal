package top.dbwxd.a;

/**
 * @author xiaodongw Date: 2018-12-20 Time: 13:35
 * @version $Id$
 */
public class StringDemo {

    public static void main(String[] args) {
        String a = "123";
        String b = "123";
        System.out.println("res:"+ a == b); //false
        System.out.println("result:"+a.equals(b)); //true
    }
}
